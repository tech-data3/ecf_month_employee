#!/bin/bash
python3 -m venv ./.venv
echo "$(pwd)/src" > "$(find "$(pwd)/.venv/lib" -type d -name "site-packages")/ecf.pth"
source ./.venv/bin/activate
pip install -r requirements.txt
deactivate
