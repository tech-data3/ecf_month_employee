# ECF

## What we do

Visualisation de donnees et generation d'un document pdf.

## Installation / Usage

Actualy only the eda is done so just launch a jupyter lab in the spec/eda dir

freeze embed anything for now.

```bash
chmod +x install.sh
./install.sh

source ./venv/bin/activate
cd spec/eda
jupyter lab
```

## Run testsuit

```bash
chmod +x testsuit.sh
./testsuit.sh
```