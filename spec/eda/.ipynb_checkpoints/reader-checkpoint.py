import pathlib
import re

import python_calamine
import unidecode
import pandas as pd


class XLReader:
    path = pathlib.Path.cwd()

    def __init__(self, path, sheetname=None, header_line=None):
        self.path = XLReader.path.joinpath(pathlib.Path(path))
        if self.path.is_file():
            self.file = open(self.path, "rb")
            self.wb = python_calamine.CalamineWorkbook.from_filelike(self.file)
            self.dfs = []
            if sheetname is not None:
                self.set_sheet(sheetname, header_line)
        else:
            raise ValueError(f"{self.path} n'est pas un fichier")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

    @staticmethod
    def standardize_column_name(column_name):
        column_name = str(column_name)
        if result := re.match("^(.*?)[*\n]", str(column_name)):
            column_name = result.group(1)
        column_name = str(column_name)
        return unidecode.unidecode(column_name.lower().strip().replace(" ", "_").replace('\'', ''))

    def get_header(self, header_line):
        for _ in range(header_line):
            next(self.rows)
        return list(map(self.standardize_column_name, next(self.rows)))

    def get_line(self):
        for row in self.rows:
            yield dict(zip(self.header, row))
            
    def set_sheet(self, sheetname, header_line = None):
        if not header_line:
            header_line = 0
        self.sheet = self.wb.get_sheet_by_name(sheetname).to_python(skip_empty_area=False)
        self.rows = iter(self.sheet)
        self.header = self.get_header(header_line)
        
    def describe_wb(self):
        strings = [f"Sheets :"]
        strings.append(f"Number of sheet : {len(self.wb.sheet_names)}")
        strings.append(f"sheet names : {self.wb.sheet_names}")
        strings.append("")
        for j, sheet in enumerate(self.wb.sheet_names):
            header_line = 0
            if j:
                header_line = 1
            strings.append("--------")
            strings.append(f"{sheet}")
            
            self.set_sheet(sheet, header_line=header_line)
            strings.append(f"{self.header}")
            strings.append(f"max_row : {len(self.sheet)}, max_col : {len(self.sheet[0])}")
            strings.append("")
            self.dfs.append(pd.DataFrame.from_records(self.get_line()))
            print(self.dfs[-1])
                

        return "\n".join(strings)
        
