import pathlib
import re

import python_calamine
import unidecode


class XLReader:
    path = pathlib.Path.cwd()

    def __init__(self, path, sheetname=None, header_line=0, max_col=-1):
        self.path = XLReader.path.joinpath(pathlib.Path(path))
        if self.path.is_file():
            self.file = open(self.path, "rb")
            self.wb = python_calamine.CalamineWorkbook.from_filelike(self.file)
            if sheetname is not None:
                self.set_sheet(sheetname, header_line)
        else:
            raise ValueError(f"{self.path} n'est pas un fichier")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.file.close()

    @staticmethod
    def standardize_column_name(column_name):
        column_name = str(column_name)
        if result := re.match("^(.*?)[*\n]", str(column_name)):
            column_name = result.group(1)
        column_name = str(column_name)
        return unidecode.unidecode(column_name.lower().strip().replace(" ", "_").replace('\'', ''))

    def get_header(self, header_line):
        for _ in range(header_line):
            next(self.rows)
        return list(map(self.standardize_column_name, next(self.rows)[:self.max_col] if self.max_col >= 0 else next(self.rows)))

    def get_line(self, format:dict = None):
        for row in self.rows:
            yield {k: format[k](v) if format and k in format else v for k, v in zip(self.header, row[:self.max_col] if self.max_col >= 0 else row)}

    def set_sheet(self, sheetname, header_line = 0, max_col = -1):
        self.sheet = self.wb.get_sheet_by_name(sheetname).to_python(skip_empty_area=False)
        self.rows = iter(self.sheet)
        self.max_col = max_col
        self.header = self.get_header(header_line)

    @property
    def sheet_names(self):
        return self.wb.sheet_names


