# spec

## activity diagram

```plantuml
@startuml
start
repeat
group reader
  :get iterator on file;
end group

group transformer
  :make df;
end group
repeat while (is there more sheet to parse) is (yes)
->no;
group transformer
  :set transform function list;
end group
repeat
group transformer
  :call transform function on df;
  :make answer;
  :make infography;
end group
group writer
  :transform as latex;
  :append to writer;
end group
repeat while (there is more transform function) is (yes)
->no;
group writer
:write in file;
end group
stop
@enduml
```